import csv
import nltk
from nltk import word_tokenize, collections
from nltk.util import ngrams
from collections import Counter
datasetrating={}
unigram={}
bigram={}
with open('imdb_master.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    line_count = 0
    with open('output.txt', 'w') as outfile:
       for row in csv_reader:
            if (line_count>50000):
               datasetrating[row[2]]=row[3]
               line=row[2]
               line = line.replace("\n", " ")
               line = line.replace("<br />", " ")
               line = line.replace("<br /><br />", " ")
               line = line.replace("<s>"," ")
               outfile.write(line)
            line_count = line_count + 1
       outfile.close()
csv_file.close()
with open('output.txt', 'r') as outfile:
  data= outfile.read()

  token = nltk.word_tokenize(data)
  unigrams = ngrams(token, 1)
  bigrams=ngrams(token,2)
  #print(Counter(unigrams))
  dataset2=collections.Counter(bigrams)


  dataset =collections.Counter(unigrams)

  for key, value in dataset.items():
      x = str(key)
      x = x.rstrip("',)")
      x = x.lstrip("('")
      x = x.lstrip('"')
      x = x.rstrip('"')
      x = x.rstrip('.')
      x = x.rstrip(',')
      x = x.lstrip('.')
      x = x.lstrip(',')
      if (value>5 and x!="." and x!=","):
        unigram[x]=value

  for key, value in dataset2.items():
      if(value>5):
        x = str(key)
        x = x.split(',')
        x[0] = x[0].lstrip("('")
        x[0] = x[0].rstrip(" ' ")
        x[0] = x[0].rstrip(")")
      # x[0] = x[0].lstrip('"')
        x[0] = x[0].rstrip(" ")
        x[0] = x[0].lstrip(" ")
        x[1] = x[1].lstrip(" '")
        x[1] = x[1].rstrip("')")
        x[1] = x[1].rstrip(")")
        x[1] = x[1].lstrip("(")
        # x[1] = x[1].lstrip('"')
        x[1] = x[1].rstrip(" ")
        x[1] = x[1].lstrip(" ")
        x[0]=x[0].lstrip('"')
        x[0] = x[0].rstrip('"')
        x[0] = x[0].rstrip('.')
        x[0] = x[0].rstrip(',')
        x[0] = x[0].lstrip('.')
        x[0] = x[0].lstrip(',')
        x[1] = x[1].lstrip('"')
        x[1] = x[1].rstrip('"')
        x[1] = x[1].rstrip('.')
        x[1] = x[1].rstrip(',')
        x[1] = x[1].lstrip('.')
        x[1] = x[1].lstrip(',')
        if(x[0]!="." and x[0]!="." and x[1]!="." and x[1]!=","):
            bigram[x[0], x[1]] = value
print(unigram.items())
print(bigram)









